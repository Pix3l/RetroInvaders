CC = gcc
CFLAGS = -Wall
LDFLAGS = -lSDL -lSDL_mixer
OBJ = main.o util.o fps.o mixer.o ufo.o invaders.o bunker.o bullet.o player.o draw.o timer.o game.o
OUT = retroinvaders

main: $(OBJ)
	$(CC) $(CFLAGS) -o $(OUT) $(OBJ) $(LDFLAGS)
clean:
	$(RM) $(OBJ) $(OUT)
