/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include <SDL/SDL.h>
#include "fps.h"

int fps_sync (void)
{
	fps.t = SDL_GetTicks ();

	if (fps.t - fps.tl >= fps.frequency)
	{
		fps.temp = (fps.t - fps.tl) / fps.frequency;
		fps.tl += fps.temp * fps.frequency;
		return fps.temp;
	}
	else
	{
		SDL_Delay (fps.frequency - (fps.t - fps.tl));
		fps.tl += fps.frequency;
		return 1;
	}
}
