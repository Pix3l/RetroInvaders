/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, Space for shoot.		  *
 *    Destroy how many invaders as you can for make an highscore.	  *
 *    Don't let they hit you or you  								  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#ifndef _UTIL_H
#define _UTIL_H

#include <time.h>
#include <SDL/SDL.h>

#include "fps.h"
#include "mixer.h"
#include "game.h"
#include "draw.h"
#include "timer.h"
#include "player.h"
#include "invaders.h"
#include "bullet.h"
#include "ufo.h"
#include "bunker.h"

#define SCREEN_WIDTH 448
#define SCREEN_HEIGHT 480

#define TILE_SIZE 16
#define FONT_W 10
#define FONT_H 14
#define STD_H 16

#define MAX_UPPER_LIMIT 51
#define MAX_BOTTOM_LIMIT 461	//Limite massimo inferiore del capo di gioco
#define MAX_BOTTOM_ROW 400	//Riga massima a cui gli alieni possono arrivare

#define MAX_BUNKERS 80 // Numero massimo totale di tiles per i bunker
#define MAX_BULLETS 2 

#define BUNKERS_Y_POS 368

struct _Game
{
	int alienAlive, step; //step gestisce il suono per il movimento
	int hiscore;
	int status; //Game status
	int round; //Round di gioco
} Game;

enum States
{
	GAMEOVER, MENU, PREGAME, GAME, LOST, WIN
};

// Game status
// 0 - Game Over
// 1 - Menu
// 2 - Pre-Game
// 3 - Game
// 4 - Player killed
// 5 - Game end!

//Global variables
int repeat, quit;
int start_time, t2;
char message[35]; // Store in game text

SDL_Event event;

SDL_Rect rect;
SDL_Rect area;

Uint8 *keystate; // keyboard state

SDL_Surface *screen;
SDL_Surface *bmpfont;
SDL_Surface *tmpfont; // Temporary single character surface
SDL_Surface *sprite_sheet;
SDL_Surface *title;

int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2);
void init();
void cleanUp();

#endif
