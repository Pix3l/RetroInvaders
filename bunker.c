/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "bunker.h"

void drawBunker()
{
    if(Game.status<GAME) return;

	int i;
	for (i = 0; i < 80; i++)
	{
		if(Bunker[i].tile==1)
		 drawRect(Bunker[i].x, Bunker[i].y , BUNKER_TILE_W, BUNKER_TILE_H, 0xff0000);
        
        if(Bunker[i].tile==2)	//Left Up
         getSprite(sprite_sheet, screen, 50, 8, BUNKER_TILE_W, BUNKER_TILE_H, Bunker[i].x, Bunker[i].y);

        if(Bunker[i].tile==3)	//Right Up
         getSprite(sprite_sheet, screen, 70, 8, BUNKER_TILE_W, BUNKER_TILE_H, Bunker[i].x, Bunker[i].y);
        
        if(Bunker[i].tile==4)
         getSprite(sprite_sheet, screen, 50, 0, BUNKER_TILE_W, BUNKER_TILE_H, Bunker[i].x, Bunker[i].y);

        if(Bunker[i].tile==5)
         getSprite(sprite_sheet, screen, 70, 0, BUNKER_TILE_W, BUNKER_TILE_H, Bunker[i].x, Bunker[i].y);
        
        //Danneggiato
        if(Bunker[i].sprite_index==1 || Bunker[i].sprite_index==2)
         getSprite(sprite_sheet, screen, 70, 56, BUNKER_TILE_W, BUNKER_TILE_H, Bunker[i].x, Bunker[i].y);
	}
}

/**
 * void hitBunker(int)
 * 
 * Controlla per eventuali collisioni tra pezzi di bunker e proiettili
 * 
 * @param int bullet
 *      Indice del proiettile da controllare, 0 Giocatore - 1 Invasori
 
**/
void hitBunker(int bullet)
{
	int i;

	for(i=0; i<MAX_BUNKERS; i++)
	{
		if (Bunker[i].tile>0)
		 if(rectCollision(Bullet[bullet].x, Bullet[bullet].y, BULLET_W, BULLET_H,
						  Bunker[i].x, Bunker[i].y, BUNKER_TILE_W, BUNKER_TILE_H))
		 {
			 Bunker[i].sprite_index--;
			 Bullet[bullet].alive = 0;
			 if(Bunker[i].sprite_index==0)
			  Bunker[i].tile = 0;
			
			 break;
		 }
	}
}

void initBunker(int x, int y)
{
	int xx, yy;
	static int b=0;
	
	if(b<MAX_BUNKERS)
	{
		for (yy=0; yy<4; yy++)
		{
			for (xx=0; xx<5; xx++)
			{
				Bunker[b].x = (xx*BUNKER_TILE_W) + x;
				Bunker[b].y = (yy*BUNKER_TILE_H) + y;
				Bunker[b].tile = (yy==3&&xx==2) ? 0 : 1;
				
				if (yy==0&&xx==0)
				 Bunker[b].tile = 2;
				else if (yy==0&&xx==4)
				 Bunker[b].tile = 3;
                else if (yy==3&&xx==1)
                 Bunker[b].tile = 4;
                else if (yy==3&&xx==3)
                 Bunker[b].tile = 5;
				
				if(Bunker[b].tile>0)
			     Bunker[b].sprite_index = 3;
			     
				b++;
				
			}
		}
	}
	if(b==MAX_BUNKERS)
	 b=0;
}
