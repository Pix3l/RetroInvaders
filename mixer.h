/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#ifndef _MIXER_H
#define _MIXER_H

#include <SDL/SDL_mixer.h>

Mix_Chunk *sound;		//Pointer to our sound, in memory
int channel;			//Channel on which our sound is played
int audio_rate;			//Frequency of audio playback
Uint16 audio_format; 	//Format of the audio we're playing
int audio_channels;		//2 channels = stereo
int audio_buffers;		//Size of the audio buffers in memory

void initMixer();
void playStep();
void playSound(char *file);

#endif
