/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, Space for shoot.		  *
 *    Destroy how many invaders as you can for make an highscore.	  *
 *    Don't let they hit you or you  								  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#ifndef _INVADERS_H
#define _INVADERS_H

#include "util.h"

/** Sprites width **/
#define	INV_ONE 16
#define INV_TWO 22
#define INV_THREE 24

#define MAX_ALIENS 55 // Numero massimo di invasori alieni

struct _Enemy
{
    int sprite_index;
	int x, y, alive, type, dirX, width;
	int timer;
} Enemy[MAX_ALIENS];

void invertDirection(int dir);
void getDown();
int checkBorder();
void moveInvaders();
void drawInvaders();
void initInvaders();

#endif
