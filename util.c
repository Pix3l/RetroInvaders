/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, Space for shoot.		  *
 *    Destroy how many invaders as you can for make an highscore.	  *
 *    Don't let they hit you or you  								  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "util.h"

//Object-to-object bounding-box collision detector
int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2)
{
	if (y1+h1 < y2) return 0;
	if (y1 > y2+h2) return 0;
	if (x1+w1 < x2) return 0;
	if (x1 > x2+w2) return 0;

	return 1;
}

void init()
{
    fps.t=0;
    fps.tl=0;
    fps.frequency=1000 / 100;

    quit = 0;
    
    Game.hiscore = 5000;
    Game.status = MENU;
    Game.round = 1;
    
	initInvaders();
	initPlayer();
	initBunker(64, BUNKERS_Y_POS);
	initBunker(154, BUNKERS_Y_POS);
	initBunker(244, BUNKERS_Y_POS);
	initBunker(334, BUNKERS_Y_POS);
	
    // Initialize the bullet
	Bullet[0].alive = 0;
	Bullet[1].alive = 0;
	
	initUfo();
	
	// Load bmp font on a surface
    bmpfont=loadImage("fnt.bmp");
    
    // Create a temporary surface for single bmp font character
    tmpfont=SDL_CreateRGBSurface(SDL_SWSURFACE,FONT_W,FONT_H,32,0,0,0,0);
    if(tmpfont == NULL) {
        fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
        exit(1);
    }

    sprite_sheet=loadImage("spritesheet.bmp");
    title=loadImage("title.bmp");
}

/**
 * void cleanUp()
 * 
 * Libera le risorse precedentemente allocate in memoria e termina
 * correttamente le librerie.
 *
 **/
void cleanUp()
{
    SDL_FreeSurface(screen);
    SDL_FreeSurface(bmpfont);
    SDL_FreeSurface(tmpfont);
    SDL_FreeSurface(sprite_sheet);
    SDL_FreeSurface(title);
    Mix_FreeChunk(sound);
    Mix_CloseAudio();
    SDL_Quit();
}
