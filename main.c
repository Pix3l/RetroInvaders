/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "util.h"

int main(int argc, char *argv[])
{
  // init video stuff
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	atexit(SDL_Quit);

   // init screen
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_SWSURFACE);
	if(screen == NULL)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	
	initMixer();
	
	SDL_WM_SetCaption("Retro Invaders 1.1", "Retro Invaders 1.1");
	
	init();
	
	//main game loop
	while(!quit)
	{
	   SDL_PollEvent(&event);
		repeat = fps_sync ();
		int i;
		for (i = 0; i < repeat; i ++)
		 {
			 
			if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
				quit = 1; /* Window closed */
				
			doGame();
		 }
		
		// Cancella tutto lo schermo di gioco
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
		draw();
		// Aggiorna lo schermo di gioco
		SDL_Flip(screen);
	}

	return 0;
}
