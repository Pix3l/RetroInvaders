/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "timer.h"

/**
 * int getSeconds(int)
 * 
 * Calcola la differenza di tempo tra quello attuale e quello passato
 * come argomento, ritornandone il valore espresso in secondi.
 * 
 * @param int lastTick
 *      Momento dal quale comcinare a contare il tempo trascorso
 * @return int seconds
 *      Tempo passato dall'ultimo tick specificato
 * 
 **/
int getSeconds(int lastTick)
{
    int seconds = (fps.t - lastTick)/1000;
    return seconds;
}

int getMilliSeconds(int lastTick)
{
    int milliseconds = (fps.t - lastTick);
    return milliseconds;
}
