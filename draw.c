/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, Space for shoot.		  *
 *    Destroy how many invaders as you can for make an highscore.	  *
 *    Don't let they hit you or you  								  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "draw.h"

/**
 * Carica un'immagine BMP specifica in una superficie e ne converte il 
 * formato in quello attuale dello schermo per un blitting piu' rapido
 * 
 * @param Char *file
 *      Percorso e nome del file immagine BMP da caricare
 * 
 * @return SDL_Surface *surface
 *      Superficie contentente l'immagine caricata e convertita nel
 * 		formato attuale dello schermo
 **/
SDL_Surface * loadImage(char *file)
{
	SDL_Surface *temp1, *temp2;
	
	temp1 = SDL_LoadBMP(file);
	
	if (temp1 == NULL)
	{
	 printf("Can't load image: %s\n", SDL_GetError());
	 SDL_FreeSurface(temp1);
	 exit(1);
	} 
	
	temp2 = SDL_DisplayFormat(temp1);
	SDL_FreeSurface(temp1);
	return temp2;
}

/**
 * Prende un tile di dimensioni specificate da una superficie
 * 
 * @param SDL_Surface *src
 *      Superficie da cui prelevare il tile
 * @param SDL_Surface *dest
 *      Superficie di destinazione su cui applicare il tile
 * @param int xspr, int yspr
 *      Coordinate da cui si iniziera' a prelevare il tile
 * @param int w, int h
 *      Larghezza e grandezza del tile
 * @param int x, int y
 *      Coordinate a cui verra' disegnato il tile sulla superficie
 *      di destinazione
 **/
void getSprite(SDL_Surface *src, SDL_Surface *dest, int xspr, int yspr, int w, int h, int x, int y)
{
    SDL_Rect pick;
    pick.x=xspr;
    pick.y=yspr;
    pick.w=w;
    pick.h=h;
    
    SDL_Rect area;
    area.x=x;
    area.y=y;
    area.w=w;
    area.h=h;
    
    SDL_BlitSurface(src,&pick,screen,&area);
}

/**
 * Ritorna il valore in bit di un pixel preso da una superficie 
 * specificata
 * 
 * @param SDL_Surface *surface
 *      Superficie da cui prelevare il tile
 * @param int x, int y
 *      Coordinate a cui leggere il pixel
 * @return Uint *p
 * 		Ritorna un puntatore al pixel, di tipo unsigned int della 
 * 		dimensione in byte del pixel
 **/
Uint32 get_pixel(SDL_Surface *surface, int x, int y)
{
	int bpp = surface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
	
	switch (bpp)
	{
		case 1:
		return *p;

		case 2:
		return *(Uint16 *)p;

		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
		
		case 4:
			return *(Uint32 *)p;
		
		default:
			return 0;
	}
}

void put_pixel(SDL_Surface *_ima, int x, int y, Uint32 pixel)
{
	int bpp = _ima->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)_ima->pixels + y * _ima->pitch + x*bpp;

	switch (bpp)
	{
		case 1:
			*p = pixel;
			break;
			
		case 2:
			*(Uint16 *)p = pixel;
			break;
			
		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			{
				p[0]=(pixel >> 16) & 0xff;
				p[1]=(pixel >> 8) & 0xff;
				p[2]=pixel & 0xff;
			}
			else
			{
				p[0]=pixel & 0xff;
				p[1]=(pixel >> 8) & 0xff;
				p[2]=(pixel >> 16) & 0xff;
			}
			break;
			
		case 4:
			*(Uint32 *) p = pixel;
			break;
	}
}

/**
 * Rimpiazza un colore specifico (target) con uno un rimpiazzo (replecement)
 * in una zona ben precisa di una determinata superficie pixel per pixel
 * 
 * @param SDL_Surface *src
 *      Puntatore alla superficie su cui agire
 * @param Uint32 target
 *      Colore RGB da sostituire
 * @param Uint32 replacement
 *      Colore RGB di rimpiazzo
 **/
void replaceColor (SDL_Surface * src, Uint32 target, Uint32 replacement)
{
	int x;
	int y;
	
	if (SDL_MUSTLOCK (src))
	{
		if (SDL_LockSurface (src) < 0)
			return;
	}
	
	for (x = 0; x < src->w; x ++)
	{
	    //~ printf("X %d\n", x);
		for (y = 0; y < src->h; y ++)
		{
		    //~ printf("Y %d\n", y);
			if (get_pixel (src, x, y) == target)
             put_pixel (src, x, y, replacement);
		}
	}

	if (SDL_MUSTLOCK (src))
		SDL_UnlockSurface (src);
}

/**
 * Copia una porzione dalla superficie bmpfont e la copia sulla superficie screen
 * 
 * @param SDL_Surface *screen
 *      Puntatore alla superficie di destinazione
 * @param SDL_Surface *bmpfont
 *      Puntatore alla superficie di origine
 * @param int X, int Y
 *      Destinazione in cui si andra' a copiare il contenuto di origine
 * @param int w, int h
 *      Rispettivamente, larghezza e grandezza della superficie da copiare
 * @param int asciicode
 *      Singolo carattere convertito in un equivalente decimale
 * @param SDL_Color color
 *      Struttura contenente i valori RGB di colore da usare per la superficie
**/
void drawChar(SDL_Surface *screen, SDL_Surface *bmpfont, int X, int Y, int w, int h, int asciicode, SDL_Color color)
{
     SDL_Rect pick, tmpfontrect;
     
     //Cordinate nella superficie con le lettere
     pick.x=(asciicode % 32)*w;
     pick.y=(asciicode / 32)*h;
     pick.w=w;
     pick.h=h;
     area.x=X;
     area.y=Y;
     area.w=w;
     area.h=h;
     
     tmpfontrect.x=0;
     tmpfontrect.y=0;
     tmpfontrect.w=8;
     tmpfontrect.h=16;
     
     SDL_BlitSurface(bmpfont,&pick,tmpfont,NULL);
     
     replaceColor (tmpfont, SDL_MapRGB (tmpfont->format, 255, 255, 255), 
                   SDL_MapRGB (tmpfont->format, color.r, color.g, color.b));
     
     SDL_SetColorKey(tmpfont, SDL_SRCCOLORKEY, SDL_MapRGB(tmpfont->format, 0, 0, 0) ); 
     
     //int SDL_BlitSurface(SDL_Surface *src, SDL_Rect *srcrect, SDL_Surface *dst, SDL_Rect *dstrect);
     SDL_BlitSurface(tmpfont,NULL,screen,&area);
}

void drawString(SDL_Surface *screen, int X, int Y, char text[], SDL_Color color) {
  int i=0;
  int asciicode;
  area.x=X; 
  area.y=Y; 

  //Per la lunghezza della stringa
  for (i=0;i<35;i++){
      asciicode=text[i]; //Valori decimali del carattere
      //~ printf("%d ascii\n", asciicode);
      if (asciicode == 0 ) {
                    break;
      }
      // 8x16 dimensioni della lettera
      drawChar(screen, bmpfont, area.x, area.y, FONT_W, FONT_H, asciicode, color);    
      area.x=area.x+TILE_SIZE;
  }
}

void drawRect(int x, int y, int width, int height, int color)
{
    rect.x = x;
    rect.y = y;
    rect.w = width;
    rect.h = height;
    SDL_FillRect(screen, &rect, color);
}

void drawWin()
{
    if(Game.status!=WIN) return;
    
    sprintf(message,"YOU WIN! THE EARTH IS SAFE!");	
	drawString(screen, 16, 242, message, cyan);
	sprintf(message,"THANKS FOR PLAYING!");	
	drawString(screen, 80, 290, message, cyan);
    sprintf(message,"* PIX3LWORKSHOP *");	
	drawString(screen, 98, 416, message, purple);
}

void drawGameOver()
{
    if(Game.status!=GAMEOVER) return;
    
    sprintf(message,"PLAYER GAME OVER");
	drawString(screen, 108, 258, message, cyan);
}

void drawPreGame()
{
    if(Game.status!=PREGAME) return;
    
    sprintf(message,"PLAYER READY?");	
	drawString(screen, 124, 258, message, cyan);
}

void drawTitle()
{
    if(Game.status!=MENU) return;
    
    getSprite(title, screen, 0, 0, 374, 222, 34, 98);

		sprintf(message,"*PRESS 1 TO PLAY*");	
		drawString(screen, 98, 366, message, white);
		
    sprintf(message,"* PIX3LWORKSHOP *");	
	drawString(screen, 98, 416, message, cyan);
}

void draw()
{
	drawGameOver();
	if(Game.status==GAMEOVER) return;
	drawWin();
	if(Game.status==WIN) return;

    drawTitle();
    drawPreGame();
	
    drawInvaders();
    drawPlayer();
    drawBunker();
    drawUfo();
    drawBullet();
    
    sprintf(message,"SCORE");	
	drawString(screen, 50, 4, message, cyan);
	sprintf(message,"%05d", Player.score);	
	drawString(screen, 50, 33, message, white);
	
    sprintf(message,"HI-SCORE");	
	drawString(screen, 162, 2, message, red);
	sprintf(message,"%05d", Game.hiscore);	
	drawString(screen, 178, 34, message, green);
	
    sprintf(message,"ROUND");	
	drawString(screen, 338, 2, message, yellow);
	sprintf(message,"%02d", Game.round);	
	drawString(screen, 354, 34, message, white);
	
    sprintf(message,"FREE PLAY");	
	drawString(screen, 274, 464, message, purple);
	
	if(Game.status==MENU) return;
	
	// Scenario
	drawRect(0, MAX_BOTTOM_LIMIT, 462, 1, 0xFF0000);
	// Player lifes
    sprintf(message,"%d", Player.lives);	
	drawString(screen, 9, 464, message, cyan);
	
	int i, x=42;
	for(i=Player.lives; i>0; i--)
	{
		getSprite(sprite_sheet, screen, 0, 16, PLAYER_W, PLAYER_H, x, 462);
		x+=PLAYER_W+4;
	}
}

void initDraw()
{
	/** Colors **/
	//~ white = WHITE;
	//~ cyan = CYAN;
	//~ blue = BLUE;
	//~ yellow = YELLOW;
	//~ purple = PURPLE;
	//~ red = RED;
	//~ green = GREEN;
}
