/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, Space for shoot.		  *
 *    Destroy how many invaders as you can for make an highscore.	  *
 *    Don't let they hit you or you  								  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "invaders.h"

void invertDirection(int dir)
{
	int i;
	
	for(i=0; i<MAX_ALIENS; i++)
	{
	 Enemy[i].dirX = dir;
	}
}

void getDown()
{
	int i;
	
	for(i=0; i<MAX_ALIENS; i++)
	{
		Enemy[i].y+= STD_H;
	}
}

/**
 * int checkBorder()
 * 
 * Controlla se almeno uno degli invasori ha toccato uno dei bordi
 * dello schermo.
 * 
 * @return 
 *      0 nessuna collisione - 1 Collisione avvenuta
 **/
int checkBorder()
{
    int i;
    
    for(i=0; i<MAX_ALIENS; i++)
		{
		 if (Enemy[i].alive)
		 {
			if (Enemy[i].x + Enemy[i].width >= SCREEN_WIDTH - 4 && Enemy[i].dirX != -1)
			{
				// Invert Direction
				invertDirection(-1);
				
				getDown();
				return 1;
			}
			else if (Enemy[i].x - (Enemy[i].width/2) < 0 && Enemy[i].dirX != 1)
			{
				// Invert Direction
				invertDirection(1);
				getDown();
				return 1;
			}
			
		 }
		}
		return 0;
}

void dropBullet()
{
	int random=0;
	
	// Spariamo un colpo al giocatore
	random = (rand() % MAX_ALIENS);
	
	if(random+11<MAX_ALIENS)
	{
		if(Enemy[random+11].alive==0)
		{
		  if(Bullet[1].alive==0 && Enemy[random].alive==1)
		  {
		   Bullet[1].alive = 1;
		   Bullet[1].x = Enemy[random].x + TILE_SIZE /2 - BULLET_W/2;
		  
		   Bullet[1].y = Enemy[random].y + TILE_SIZE;
		  }
		}
	}
	
}

void moveInvaders()
{
	int i=0, k=0;
	
	if(fps.t > t2 +(20*(Game.alienAlive)))
	 {
	 	playStep();
	 	
	     if(!checkBorder())
	     {
            // Muoviamo gli invaders
            for(i=0; i<MAX_ALIENS; i++)
            {
                
                if(Enemy[i].alive)
                {
                    if(Enemy[i].sprite_index==0)
                     Enemy[i].sprite_index=1;
                    else if(Enemy[i].sprite_index==1)
                     Enemy[i].sprite_index=0;
                    
                    // Controlliamo le collisioni con i bunker
                    for(k=0; k<MAX_BUNKERS; k++)
                    {
                         if(rectCollision(Enemy[i].x, Enemy[i].y, Enemy[i].width, STD_H, Bunker[k].x, Bunker[k].y, BUNKER_TILE_W, BUNKER_TILE_H))
                         {
                              Bunker[k].tile = 0;
                              Bunker[k].sprite_index=0;
                         }
                    }
                    
                    if(Enemy[i].y+8 >=MAX_BOTTOM_ROW)
                    {
                    	Player.lives=1;
                    	hitPlayer();
                    	break;
					}

					// Moviamoli orizzontalmente
                    Enemy[i].x += (TILE_SIZE/2) * Enemy[i].dirX;
                }
            }
         }
		
		 dropBullet();
		
      t2 = fps.t;
	 }
}

void drawInvaders()
{
    if(Game.status<GAME) return;
    
	int i;
    int spr_index=0;	//Sprite index 2 significa sconfitto
    
	for(i=0; i<MAX_ALIENS; i++)
	 {
	 	if(Enemy[i].alive)
	 	{
	 	    if(Enemy[i].sprite_index==1)
	 	    {
	 	     spr_index=Enemy[i].width;
            }
            else
             spr_index=0;
	 	     
	 		if(Enemy[i].type==0)	//1st and 2nd line green
	 		{
	 		 getSprite(sprite_sheet, screen, spr_index, 32, Enemy[i].width, STD_H, Enemy[i].x, Enemy[i].y);
             
             if(Enemy[i].sprite_index==2)
              getSprite(sprite_sheet, screen, 32, 32, Enemy[i].width, STD_H, Enemy[i].x, Enemy[i].y);
            }
            
	 		if(Enemy[i].type==1)	//Crabs
	 		{
	 		 getSprite(sprite_sheet, screen, spr_index, 48, Enemy[i].width, STD_H, Enemy[i].x, Enemy[i].y);
	 		
             if(Enemy[i].sprite_index==2)
              getSprite(sprite_sheet, screen, 44, 48, Enemy[i].width+4, STD_H, Enemy[i].x, Enemy[i].y);
            }
            
	 		if(Enemy[i].type==2)	//Onions
	 		{
	 		 getSprite(sprite_sheet, screen, spr_index, 64, Enemy[i].width, STD_H, Enemy[i].x, Enemy[i].y);
	 		 
             if(Enemy[i].sprite_index==2)
              getSprite(sprite_sheet, screen, 48, 64, Enemy[i].width, STD_H, Enemy[i].x, Enemy[i].y);
            }
            
            if(Enemy[i].sprite_index==2 && getMilliSeconds(Enemy[i].timer) > 80)
            {
                Enemy[i].alive = 0;
                Enemy[i].timer = 0;
            } 
		}
   }

}

void initInvaders()
{
    int i, j, index=MAX_ALIENS-1, type=2;
    
    //Aliens start coord
    int starty=96+(STD_H*Game.round);
    int width=INV_THREE;
	
	int distance = 8;
	
	for (i = 5; i >0; i--)
	{
		 if(!(i%2))
		   type--;
		 
         if(i==2)
          width=INV_ONE;
         if(i==4)
          width=INV_TWO;
         
		for (j = 11; j >0; j--)
		{	
			Enemy[index].x=j*(INV_ONE*2)+distance;
			Enemy[index].y=starty+i*(STD_H*2);
			Enemy[index].alive=1;
			Enemy[index].dirX=1;
			Enemy[index].type=type;
			Enemy[index].width=width;
			Enemy[index].sprite_index=0;

			if(Enemy[index].type==0)
          	 Enemy[index].x+=1;
          	 
			index--;
		}
    }
  
  Game.alienAlive = MAX_ALIENS;	
}
