/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "player.h"

void hitPlayer()
{
	playSound("snd/explosion.wav");
	Player.sprite_index = 1;
	Player.timer = fps.t; //Il momento in cui viene colpito
	Player.lives--;
	Bullet[1].alive = 0;
	Ufo.alive = 0;
	Game.status = LOST;
}

void shootPlayerBullet()
{
    static int bulletTime;
     
    if(bulletTime+700< SDL_GetTicks())
    {
     if(!Bullet[0].alive)
     {
        Bullet[0].alive=1;
        Bullet[0].x = Player.x + (PLAYER_W/2)-1;
        Bullet[0].y = Player.y;
        Player.bullets++;
        playSound("snd/shoot.wav");
     }
     bulletTime=SDL_GetTicks();
    }
}

void movePlayer()
{
	if(Player.x > 0 && Player.incX < 0)
	{
	  Player.x+=Player.incX;
	}

	if(Player.x+PLAYER_W < SCREEN_WIDTH && Player.incX > 0)
	{
	  Player.x+=Player.incX;
	}   
}

void initPlayer()
{
    Player.status = 1;
	Player.x = SCREEN_WIDTH / 2 - (TILE_SIZE);
	Player.y = PLAYER_STARTY;
	Player.bullets = 0;
	Player.lives = 3;
	Player.sprite_index = 0;
}

void drawPlayerKilled()
{
    Bullet[0].alive = Bullet[1].alive = 0;
    
    static int index = 0;
    static int delay = 10;

    if (delay < 1)
	{
		delay = 10;
	
		if (index>=2)
			index = 0;
		else
			index++;
	}
	else
		delay --;

    // Set 200ms of delay
    if(index==2)
    {
        getSprite(sprite_sheet, screen, 26, 16, PLAYER_W, PLAYER_H, Player.x, Player.y);
    }
    else
    {
        getSprite(sprite_sheet, screen, 52, 16, PLAYER_W, PLAYER_H, Player.x, Player.y);
    }

    if(getSeconds(Player.timer) >= 2)
    {
     Player.x = SCREEN_WIDTH / 2 - (TILE_SIZE);
     Player.sprite_index=0;
     timer.start_time=fps.t; //Per il timing di pre-game
    }
}
 
void drawPlayer()
{
    if(Game.status<GAME) return;
    
    // Draw players
    if(!Player.sprite_index)
     getSprite(sprite_sheet, screen, 0, 16, PLAYER_W, PLAYER_H, Player.x, Player.y);
    else             
     drawPlayerKilled();
}
