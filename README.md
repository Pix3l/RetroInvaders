# RetroInvaders
### A free and simple, open source SDL based **Space Invaders** clone.

##### Release 1.1 - 2011 - [pix3lworkshop.altervista.org](pix3lworkshop.altervista.org)

========================================================================

## Release History

* Sep 9, 2011 - Release 1.0
* Dec 23, 2011 - Release 1.1

## Requirements

This game requires **SDL 1.2** and **SDL 1.2 Mixer** extension to be compiled and executed.  
You should found those libraries in your favorite distro repository or you can find it at [https://www.libsdl.org/](https://www.libsdl.org/)

###	Compile

You can compile the source code with gcc using this command

```
make
./retroinvaders
```

### Instructions

**Arrow keys** moves the player, **F** key for shoot.
		
Destroy how many aliens as you can to make an highscore and save the 
heart trough 5 levels!  
Don't let they hit you or you will lose a life, 3 life per play.  
If enemy get too down to you, it will result in an instant game over!

## Assets

Graphical assets are released under under the [Attribution-NonCommercial-NoDerivs 3.0 Unported (CC BY-NC-ND 3.0)](https://creativecommons.org/licenses/by-nc-nd/3.0/).

Sounds effects are provided from [classicgaming.cc](http://www.classicgaming.cc/classics/space-invaders/sounds), but not covered under any license of this project.

## License

This game is released as open source under [GPL 2.0 license](https://www.gnu.org/licenses/gpl-2.0.html), You should have received a copy of the **GNU General Public License along with this program;** if not, write to the **Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.**

This game is provided without warranty, use it at your own risk!

[pix3lworkshop.altervista.org](pix3lworkshop.altervista.org)
