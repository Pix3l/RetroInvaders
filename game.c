/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "game.h"

void doBullet()
{
	movePlayerBullet();
	moveInvaderBullet();
}

void win()
{
	if (event.key.keysym.sym == SDLK_RETURN)
    {
        Game.status=MENU;
        init();
    }
}

void gameover()
{
	if (event.key.keysym.sym == SDLK_RETURN)
    {
        Game.status=MENU;
        init();
    }
}

/**
 * void preGame()
 * 
 * Questa funzione gestisce uno screen di pre-game.
 * Permette al giocatore di prendersi una manciata di secondi per
 * prepararsi prima di iniziare il gioco vero e proprio.
 **/ 
void preGame()
{
    if(getSeconds(timer.start_time) > 1)
    {
        //Reset generic timer
        timer.start_time = 0;
        //Let's play!
        Game.status=GAME;
    }
}

void titleScreen()
{
    if (event.key.keysym.sym == SDLK_1)
    {
        Player.status = 1;
        timer.start_time = fps.t;
        Game.status=PREGAME;
    }
}

void doLogic()
{
    //Se entrambi i giocatori hanno fatto game over il gioco termina
    if(!Player.status)
    {
    	Game.status = GAMEOVER;
    	Player.score=0;
    }
    
    // Se il numero delle vite scende a zero e l'animazione 
    // di esplosione e' terminata si ricomincia
    if(Player.lives <1 && Player.sprite_index==0)
    {
    	Game.status = GAMEOVER;
     	Player.score=0;
    }
    else if(Player.sprite_index==0)
    {
    	Game.status = PREGAME;
	}
}

void getInput()
{
    // Grab a keystate snapshot
    keystate = SDL_GetKeyState( NULL );
    /* Handle key presses */
     if (keystate[SDLK_LEFT] && (Player.x > 0))
       Player.incX=-1;
       
     if (keystate[SDLK_RIGHT] && (Player.x + PLAYER_W < SCREEN_WIDTH))
       Player.incX=+1;
       
     if(!keystate[SDLK_LEFT] && !keystate[SDLK_RIGHT])
      Player.incX=0;

     if(keystate[SDLK_f] || keystate[SDLK_SPACE])
      {
         shootPlayerBullet();
      }

     movePlayer();
}

void doGame()
{
	switch(Game.status)
	{
		case MENU:
			titleScreen();
			break;
		case PREGAME:
			preGame();
			break;
		case GAME:
			getInput();
			doBullet();
			moveInvaders();
			moveUfo();
			break;
		case LOST:
			doLogic();
			break;
		case GAMEOVER:
			gameover();
			break;
		case WIN:
			win();
			break;
	}
	srand (time(NULL));
}
