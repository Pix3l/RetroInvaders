/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#ifndef _DRAW_H
#define _DRAW_H

#include "util.h"

// SDL_color values
static const SDL_Color white = {255, 255, 255};
static const SDL_Color cyan  = {0, 255, 255};
static const SDL_Color blue  = {0, 0, 255};
static const SDL_Color yellow  = {255, 255, 0};
static const SDL_Color purple = {255, 0, 255};
static const SDL_Color red  = {255, 0, 0};
static const SDL_Color green  = {0, 255, 0};

SDL_Surface * loadImage(char *file);
void getSprite(SDL_Surface *src, SDL_Surface *dest, int xspr, int yspr, int w, int h, int x, int y);
Uint32 get_pixel(SDL_Surface *surface, int x, int y);
void put_pixel(SDL_Surface *_ima, int x, int y, Uint32 pixel);
void replaceColor (SDL_Surface * src, Uint32 target, Uint32 replacement);
void drawChar(SDL_Surface *screen, SDL_Surface *bmpfont, int X, int Y, int w, int h, int asciicode, SDL_Color color);
void drawString(SDL_Surface *screen, int X, int Y, char text[], SDL_Color color);
void drawRect(int x, int y, int width, int height, int color);
void drawWin();
void drawGameOver();
void drawPreGame();
void drawTitle();
void draw();
void initDraw();

#endif
