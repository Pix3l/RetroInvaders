/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "ufo.h"

//Quando l'ufo viene colpito
void hitUfo()
{
	if(rectCollision(Bullet[0].x, Bullet[0].y, BULLET_W, BULLET_H,
					 Ufo.x, Ufo.y, TILE_SIZE, TILE_SIZE)
	   && Ufo.alive)
	{
		playSound("snd/invaderkilled.wav");
		if(Player.bullets==23 && Ufo.bonus==0)
		{

			Player.bullets=0;
			Ufo.bonus=1;
			Ufo.points=3;
	 	}
	 	else if(Player.bullets==15 && Ufo.bonus==1)
	 	{
	 		Player.bullets=0;
	 		Ufo.points=3;
	 	}
		else
		 Ufo.points=1;
	 	
	 	Player.score +=(100*Ufo.points);
	 	
		Ufo.alive=0;
		Ufo.sprite_index=2;
		Ufo.timer = fps.t;
		Bullet[0].alive=0;
	}
}

/*
 * Per l'ufo e' stato implementato il trucco di Furer, il trucco 
 * consiste nel colpire l'ufo al 23 esimo colpo per guadagnare 300 punti
 * invece che 100, dopo la prima volta ogni 15 colpi per guadagnarne 
 * sempre 300.
 * 
 * L'ufo appare in ogni caso, che si spari o meno a intervalli casuali.
 */

void moveUfo()
{
    int random_time = rand() % 50;
    
    if(!Ufo.alive)
     if(getSeconds(random_time) % 20 == 0)
     {
         if(Game.alienAlive>8)
         {
             Ufo.alive=1;
             playSound("snd/ufo_lowpitch.wav");
         }
     }
 
    if(Ufo.alive)
    {
        Ufo.x++;
        
        if(Ufo.x+16>SCREEN_WIDTH)
        {
            Ufo.alive=0;
            Ufo.x=-TILE_SIZE*2;
        }
    }
}

void initUfo()
{
    Ufo.x = -TILE_SIZE*2-1;
	Ufo.y =  64;
	Ufo.alive = 0;
	Ufo.sprite_index=1;
}

void drawUfo()
{
    if(Game.status<GAME) return;
    
    static int delay = 150;
    
    if(Ufo.alive)
	  getSprite(sprite_sheet, screen, 0, 0, UFO_W, STD_H, Ufo.x, Ufo.y);

	if(Ufo.sprite_index==2)
	{
	 getSprite(sprite_sheet, screen, 32, 0, 18, 14, Ufo.x, Ufo.y);
	
		if(getSeconds(Ufo.timer) > 1)
		 Ufo.sprite_index=3; 
	}

	if(Ufo.sprite_index==3)
	{
		if(delay>0)
		{
			sprintf(message,"%d", Ufo.points*100);	
			drawString(screen, Ufo.x, Ufo.y, message, purple);
			delay--;
		}
		else
		{
			delay=150;
			initUfo();
	 	}
	}
}
