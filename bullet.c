/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include "bullet.h"

void movePlayerBullet()
{
	int i=0;
	
	if(Bullet[0].alive)
	{
		Bullet[0].y-=3;
		Bullet[0].sprite_index=0;
		
		for(i=0; i<MAX_ALIENS; i++)
		   if (Enemy[i].alive &&
			   rectCollision(Bullet[0].x, Bullet[0].y, BULLET_W, BULLET_H, 
				   		     Enemy[i].x, Enemy[i].y, Enemy[i].width, STD_H)
               && Enemy[i].sprite_index<2)
		   {
		   	playSound("snd/invaderkilled.wav");
			Bullet[0].alive = 0;
			Enemy[i].sprite_index = 2;
			Enemy[i].timer = fps.t;
			Game.alienAlive--;
			
			if(Game.alienAlive==0)
			{
				timer.start_time = fps.t;
				Game.round++;
				initInvaders();
				initBunker(64, BUNKERS_Y_POS);
				initBunker(154, BUNKERS_Y_POS);
				initBunker(244, BUNKERS_Y_POS);
				initBunker(334, BUNKERS_Y_POS);
				Player.x = SCREEN_WIDTH / 2 - (TILE_SIZE);
				Player.y = PLAYER_STARTY;
				Player.bullets = 0;
				Game.status=PREGAME;
				
				if(Game.round>5)
				 Game.status = WIN;
 			}
			
			if(Enemy[i].type==0)
			 Player.score+=30;
			
			if(Enemy[i].type==1)
			 Player.score+=20;
			
			if(Enemy[i].type==2)
			 Player.score+=10;
            
            if(Player.score > Game.hiscore)
                Game.hiscore = Player.score;

			continue;
		  }
		  
		if(Bullet[0].y <= MAX_UPPER_LIMIT)
		{
			Bullet[0].alive = 0;
			Bullet[0].sprite_index=1;
		}
		
        hitBunker(0);
        hitUfo();
	}
}

void moveInvaderBullet()
{
	if(Bullet[1].alive)
	{
	    Bullet[1].sprite_index=0;
        Bullet[1].y++;

        hitBunker(1);
        
        // Quando il giocatore viene colpito
        if(rectCollision(Bullet[1].x, Bullet[1].y, BULLET_W, BULLET_H,
                         Player.x, Player.y, PLAYER_W, PLAYER_H))
        {
            hitPlayer();
        }
        
        // Se i due proiettili si scontrano c'e' possibilita' che si distruggano
        if(rectCollision(Bullet[1].x, Bullet[1].y, BULLET_W*2, BULLET_H,
                         Bullet[0].x, Bullet[0].y, BULLET_W*2, BULLET_H))
        {
        	if((Bullet[1].x % TILE_SIZE) && (Bullet[1].y % TILE_SIZE))
        	{
				Bullet[0].alive=0;
				Bullet[1].alive=0;
				
				Bullet[0].x-=BULLET_W/2;
				Bullet[0].sprite_index=1;
			}
        }
	    
	    if(Bullet[1].y + 14 >= MAX_BOTTOM_LIMIT)
	    {
	        Bullet[1].alive = 0;
	        Bullet[1].sprite_index = 2;
        }
	}
}

void drawBullet()
{
    if(Game.status<GAME) return;
    
    static int bullet_delay = 10;
    
    // Draw Bullets
    // Bullet 0 player - 1 alien
    if(Bullet[0].alive)
     drawRect(Bullet[0].x, Bullet[0].y, BULLET_W, BULLET_H, 0x00FFFF);

    if(Bullet[0].sprite_index==1)
    {
     getSprite(sprite_sheet, screen, 64, 32, TILE_SIZE, TILE_SIZE, Bullet[0].x-4, Bullet[0].y);
     
     if(bullet_delay<1)
     {
     	Bullet[0].alive=0;
     	bullet_delay=10;
     	Bullet[0].sprite_index=0;
	 }
     else
     {
      bullet_delay--;
  	 }
 	}

    if(Bullet[1].alive)
     drawRect(Bullet[1].x, Bullet[1].y, BULLET_W, BULLET_H, 0xFF00FF);

    if(Bullet[1].sprite_index==2)
    {
     getSprite(sprite_sheet, screen, 48, 34, 14, 14, Bullet[1].x-6, Bullet[1].y);
     static int delay = 10;
     
     if(delay<1)
     {
     	Bullet[1].alive=0;
     	delay=10;
     	Bullet[1].sprite_index=0;
	 }
     else
     {
      	delay--;
  	 }
 	}
}
