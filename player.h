/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#ifndef _PLAYER_H
#define _PLAYER_H

#include "util.h"

#define PLAYER_W 26
#define PLAYER_H 16
#define PLAYER_STARTY 416

struct _Player
{
	int x, y, incX, incY, bullets;
	int score, credit, lives;
	int sprite_index;
	int timer;
	int status;
} Player;

void hitPlayer();
void shootPlayerBullet();
void movePlayer();
void initPlayer();
void drawPlayerKilled();
void drawPlayer();

#endif
