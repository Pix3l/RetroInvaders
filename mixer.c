/**********************************************************************
 *                                                                    *
 *                          RetroInvaders                             *
 *                                                                    *
 *              A simple SDL based Space Invaders clone               *
 *                                                                    *
 *    Instructions: Arrows move the player, destroy how many aliens   *
 *    as you can to make an highscore. Don't let they hit you or you  *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                http://code.google.com/p/pix3lworkshop              *
 *                                                                    *
 *                                                                    *
 *   Release 1.1                                                      *
 *                                                                    *
 *********************************************************************/

#include <SDL/SDL_mixer.h>
#include "mixer.h"

/**
 * void playSound(char, int)
 * 
 * Carica ed esegue uno specifico file Wav su di uno specifico canale
 * audio.
 * 
 * @param char *file
 * 		Nome del file da caricare (compreso percorso se necessario)
 **/
void playSound(char *file)
{
	//Load our WAV file from disk
	sound = Mix_LoadWAV(file);
	if(sound == NULL) {
		printf("Unable to load WAV file: %s\n", Mix_GetError());
	}
	
	//Play our sound file, and capture the channel on which it is played
	channel = Mix_PlayChannel(-1, sound, 0);
	if(channel == -1) {
		printf("Unable to play WAV file: %s\n", Mix_GetError());
	}
}

/**
 * Ad ogni movimento degli invaders corrisponde un suono, vi e' una 
 * scaletta di 4 suoni in tutto da eseguire a ripetizione
*/
void playStep()
{
	static int sound_index =0;
	char filename[30];
	
	if(sound_index>3)
	 sound_index=1;
	else
	 sound_index++;
	
	sprintf(filename, "snd/fastinvader%d.wav", sound_index);
	//~ printf("[Debug] %s\n", filename);
	playSound(filename);
}

void initMixer()
{
	sound = NULL;		//Pointer to our sound, in memory
	audio_rate = 22050;			//Frequency of audio playback
	audio_format = AUDIO_S16SYS; 	//Format of the audio we're playing
	audio_channels = 2;			//2 channels = stereo
	audio_buffers = 4096;		//Size of the audio buffers in memory
	
	//Initialize SDL_mixer with our chosen audio settings
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		printf("Unable to initialize audio: %s\n", Mix_GetError());
		exit(1);
	}
}
